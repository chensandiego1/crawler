from bs4 import BeautifulSoup

import os
import sys
import requests 
import time

import re

url="https://www.cnblogs.com/codelove/default.html?page={page}"

page=0

while True:
    page +=1
    request_url=url.format(page=page)
    response=requests.get(request_url)
    html=BeautifulSoup(response.text,'html5lib')
    
    blog_list=html.select(".forFlow .day")
    if not blog_list:
        break 
    print("fetch:",request_url)
    
    for blog in blog_list:
        title=blog.select('.postTitle a >span')[0].get_text()
        
        print("Title")
        print('------'+title+'----------');
        
        blog_url=blog.select(".postTitle a")[0]["href"]
        
        print(blog_url)
        
        date=blog.select(".dayTitle a")[0].get_text()
        print(date)
        
        des=blog.select(".postCon > div")[0].get_text()
        print(des)
        
        print('--------------------------');
        